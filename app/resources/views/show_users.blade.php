<!DOCTYPE html>
<html>
    <head>
        <title>Show Users</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <style>
            html, body, .container {
                height: 100%;
            }
            .container {
                display: table;
                vertical-align: middle;
            }
            .vertical-center-row {
                display: table-cell;
                vertical-align: middle;
            }
        </style>
    </head>
    <body>
        <a class="btn btn-default" href="/" role="button">Back</a>
        <table class="table">
          <caption>Users which got the Mail.</caption>
          <thead>
            <tr>
              <th>#ID</th>
              <th>Name</th>
              <th>Email</th>
              <th>Birthday</th>
            </tr>
          </thead>
          <tbody>
              @foreach ($users as $user)
                    <tr>
                      <th scope="row">{{ $user->id }}</th>
                      <td>{{ $user->name }}</td>
                      <td>{{ $user->email }}</td>
                      <td>{{ $user->birthday }}</td>
                    </tr>
              @endforeach
          </tbody>
        </table>
    </body>
</html>
