{!! csrf_field() !!}
<!DOCTYPE html>
<html>
    <head>
        <title>Send Newsletter</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <style>
            html, body, .container {
                height: 100%;
            }
            .container {
                display: table;
                vertical-align: middle;
            }
            .vertical-center-row {
                display: table-cell;
                vertical-align: middle;
            }
        </style>
    </head>
    <body>
           <div class="container">
    <div class="row vertical-center-row">
        <div class="col-lg-12">
            <div class="row ">
                <div class="col-xs-4 col-xs-offset-4">
                    <form method="post" action="store" class="text-center">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="text" name="min" placeholder="age from..." /><br />
                        <input type="text" name="max" placeholder="age to..." /><br />
                        <input type="text" name="newsletter_id" placeholder="newsletter id" /><br />
                        <input type="text" name="mail_quantity" placeholder="quantity of mails" /><br />
                        <input type="submit" value="send newsletter" />
                    </form>
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
    </body>
</html>
