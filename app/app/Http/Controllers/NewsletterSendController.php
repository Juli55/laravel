<?php

namespace App\Http\Controllers;

use Validator;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsletterSendController extends Controller
{
    /**
     * Show the Form to send an Newsletter
     *
     * @param  Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        return view('send_newsletter');
    }

    /**
     * Store the sent newsletter in newsletter_log
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //Validate and store the Newsletter
        $validator = Validator::make($request->all(), [
            'min' => 'required',
            'max' => 'required',
            'newsletter_id' => 'required',
            'mail_quantity' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect('/')
                        ->withErrors($validator)
                        ->withInput();
        }
        $users_send    = [];
        $min           = $request['min'];
        $max           = $request['max'];
        $newsletter_id = $request['newsletter_id'];
        $mail_quantity = $request['mail_quantity'];
        $minDate = Carbon::now()->subYears($min);
        $maxDate = Carbon::now()->subYears($max);
        $users = DB::table('users')
                        ->where('birthday', '<=', $minDate)
                        ->where('birthday', '>=', $maxDate)
                        ->get();

        $i = 0;
        foreach($users as $user) {
            if($i < $mail_quantity) {
                $newsletter_log = DB::table('newsletter_log')
                                        ->where('newsletter_id', '=', $newsletter_id)
                                        ->where('userID', '=', $user->id)
                                        ->get();
                if(empty($newsletter_log)) {
                    //insert in newsletter_log
                    DB::table('newsletter_log')->insert(
                        ['userID' => $user->id, 'newsletter_id' => $newsletter_id]
                    );
                    $users_send[] = $user;
                    $i++;
                }
            }
        }
        return view('show_users', ['users' => $users_send]);
    }
}