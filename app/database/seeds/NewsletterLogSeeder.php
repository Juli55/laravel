<?php

use Illuminate\Database\Seeder;

class NewsletterLogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//create Schema
    	$tableName = 'newsletter_log';
    	if (!Schema::hasTable($tableName)) {
	        Schema::create($tableName, function ($table) {
			    $table->increments('id');
			    $table->bigInteger('newsletter_id');
			    $table->bigInteger('userID');
			});
		}
    }
}
