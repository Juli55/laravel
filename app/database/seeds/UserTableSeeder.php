<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker 	   = Faker::create();
    	$tableName = 'users';
    	if (!Schema::hasTable($tableName)) {

    		//create Schema
    		Schema::create($tableName, function ($table) {
			    $table->increments('id');
			    $table->string('name');
			    $table->string('email');
			    $table->date('birthday');
			});

			//generate fake users
			$users = [];
			for($i = 0; $i < 50000; $i++) {
				$name 	   = $faker->name;
				$email 	   = $faker->email;
				$birthday  = $faker->date('Y-m-d','now');
				DB::table('users')->insert([
				    $users
				]);
			}
		}
    }
}
